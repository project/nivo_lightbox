<?php

/**
 * @file
 * Theme callbacks for the Nivo Lightbox module.
 */

/**
 * Returns HTML for a Nivo Lightbox image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: Associative array of image or file data, which may include "uri",
 *     "alt", "width", "height", "title" and "attributes".
 *   - id: A unique CSS ID that identifies the current image.
 *   - gallery: A boolean indicating whether the image is part of a gallery.
 *   - group: A unique CSS ID that identifies the current images's group.
 *   - hidpi: A boolean indicating whether the image has a HiDPI version.
 *   - hidpi_image_style: An optional image style for the HiDPI image.
 *   - target_image_style: An optional image style for the target image.
 *   - lightbox_image_style: An optional image style for the lightbox image.
 *   - lightbox_type: A string indicating the type of the lightbox.
 *
 * @ingroup themeable
 */
function theme_nivo_lightbox_image_formatter($variables) {
  $item = $variables['item'];
  $fid = $item['fid'];
  $id = drupal_html_id('nivo-lightbox' . '-' . $fid);

  $image = array(
    'path' => $item['uri'],
  );

  if (array_key_exists('alt', $item)) {
    $image['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }

  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  if ($variables['target_image_style']) {
    $image['style_name'] = $variables['target_image_style'];
    $target_image = theme('image_style', $image);
  }
  else {
    $target_image = theme('image', $image);
  }

  if ($variables['lightbox_image_style']) {
    $image['style_name'] = $variables['lightbox_image_style'];
    $lightbox_image = theme('image_style', $image);
  }
  else {
    $lightbox_image = theme('image', $image);
  }

  // Apply a class to lightbox links so the JavaScript has a target to use in
  // order to initialize the lightboxes.
  // Lightbox links almost certainly contain HTML, so we must notify l().
  $options = array(
    'html' => TRUE,
    'attributes' => array(
      'class' => 'nivo-lightbox',
    ),
  );

  // Use the image's title text as the gallery caption when available.
  // Do not output an empty 'title' attribute.
  if (!empty($item['title'])) {
    $options['attributes']['title'] = $item['title'];
  }

  // Set a group ID when files are being displayed in a gallery.
  if ($variables['gallery']) {
    if ($variables['group']) {
      $options['attributes']['data-lightbox-gallery'] = $variables['group'];
    }
  }

  // Set the lightbox type if one has been selected.
  if ($variables['lightbox_type']) {
    $options['attributes']['data-lightbox-type'] = $variables['lightbox_type'];
  }

  if (module_exists('retina_images')) {
    if ($variables['hidpi']) {
      $options['attributes']['data-lightbox-hidpi'] = image_style_url($variables['hidpi_image_style'], $image['path']);
    }
  }

  // Lightboxes displaying inline HTML require a link with an #anchor and a
  // <div> that has an ID matching the anchor. The <div> contents are hidden as
  // they are only displayed when the lightbox is opened.
  if ($variables['lightbox_type'] && $variables['lightbox_type'] == 'inline') {
    // Wrap the inline HTML in a <div> and append it to the end of the link.
    // Hide the <div> contents by default using an inline style.
    // Use a matching ID for both the link anchor and the <div> ID.
    $options['fragment'] = $id;
    $options['external'] = TRUE;
    $output = l($target_image, NULL, $options);
    $output .= '<div id="' . $id . '" style="display:none;">' . $lightbox_image . '</div>';
  }
  // iFrame and AJAX lightbox types do not require any special handling.
  elseif ($variables['lightbox_type'] && $variables['lightbox_type'] == 'ajax') {
    if ($variables['lightbox_image_style']) {
      // @todo: implement a custom menu callback to return complete image HTML.
      $output = l($target_image, image_style_url($variables['lightbox_image_style'], $image['path']), $options);
    }
    else {
      $output = l($target_image, file_create_url($image['path']), $options);
    }
  }
  else {
    if ($variables['lightbox_image_style']) {
      $output = l($target_image, image_style_url($variables['lightbox_image_style'], $image['path']), $options);
    }
    else {
      $output = l($target_image, file_create_url($image['path']), $options);
    }
  }

  return $output;
}

/**
 * Returns HTML for a Nivo Lightbox file field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: Associative array of image or file data, which may include "uri",
 *     "filename", "uid", "filemime", "status", "timestamp" and "type".
 *   - id: A unique CSS ID that identifies the current file.
 *   - gallery: A boolean indicating whether the file is part of a gallery.
 *   - group: A unique CSS ID that identifies the current file's group.
 *   - target_view_mode: An optional view mode for the target file.
 *   - lightbox_view_mode: An optional view mode for the lightbox file.
 *   - lightbox_type: A string indicating the type of the lightbox.
 *
 * @ingroup themeable
 */
function theme_nivo_lightbox_file_formatter($variables) {
  $item = $variables['item'];
  $fid = $item['fid'];
  $id = drupal_html_id('nivo-lightbox' . '-' . $fid);
  $file = file_load($fid);

  // Build and render the target file, usually a thumbnail, which will open the
  // lightbox when selected.
  $render = file_view_file($file, $variables['target_view_mode']);
  $target_file = drupal_render($render);

  // Apply a class to lightbox links so the JavaScript has a target to use in
  // order to initialize the lightboxes.
  // Lightbox links almost certainly contain HTML, so we must notify l().
  $options = array(
    'html' => TRUE,
    'attributes' => array(
      'class' => 'nivo-lightbox',
    ),
  );

  // Use the file's title text as the gallery caption when available.
  // The title is added by the File Entity module and has already been sanitized
  // by file_entity_file_load().
  if (!empty($file->title)) {
    $options['attributes']['title'] = $file->title;
  }

  // Set a group ID when files are being displayed in a gallery.
  if ($variables['gallery']) {
    if ($variables['group']) {
      $options['attributes']['data-lightbox-gallery'] = $variables['group'];
    }
  }

  // Set the lightbox type if one has been selected.
  if ($variables['lightbox_type']) {
    $options['attributes']['data-lightbox-type'] = $variables['lightbox_type'];
  }

  // Lightboxes displaying inline HTML require a link with an #anchor and a
  // <div> that has an ID matching the anchor. The <div> contents are hidden as
  // they are only displayed when the lightbox is opened.
  if ($variables['lightbox_type'] && $variables['lightbox_type'] == 'inline') {
    // Build and render the inline HTML (the associated file).
    $render = file_view_file($file, $variables['lightbox_view_mode']);
    $lightbox_file = drupal_render($render);

    // Wrap the inline HTML in a <div> and append it to the end of the link.
    // Hide the <div> contents by default using an inline style.
    // Use a matching ID for both the link anchor and the <div> ID.
    $options['fragment'] = $id;
    $options['external'] = TRUE;
    $output = l($target_file, NULL, $options);
    $output .= '<div id="' . $id . '" style="display:none;">' . $lightbox_file . '</div>';
  }
  // iFrame and AJAX lightbox types do not require any special handling.
  else {
    $output = l($target_file, file_create_url($file->uri), $options);
  }

  return $output;
}
